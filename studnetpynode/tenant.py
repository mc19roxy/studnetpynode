class Tenant:
    def __init__(self, tenant_number, password):
        self._tenant_number = tenant_number
        self._password = password

    @property
    def tenant_number(self):
        return self._tenant_number

    @tenant_number.setter
    def tenant_number(self, tenant_number):
        self._tenant_number = tenant_number

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password


def add_tenant(storage, tenant_number, password):
    tenant = {"tenant_number": tenant_number, 'password': password}
    if "tenants" not in storage:
        storage["tenants"] = []
    if tenant['tenant_number'] not in [item['tenant_number'] for item in storage['tenants']]:
        storage["tenants"].append(tenant)
    storage.sync()


def remove_tenant(storage, tenant_number):
    storage['tenants'] = [tenant for tenant in storage['tenants'] if not tenant['tenant_number'] == tenant_number]
    storage.sync()


def update_tenant(storage, update_obj):
    new_tenant = update_obj['tenant']
    old_tenant_id = update_obj['id']
    old_tenant = \
        next((index for (index, tenant) in enumerate(storage['tenants']) if tenant['tenant_number'] == old_tenant_id), None)
    if new_tenant['tenant_number'] not in [item['tenant_number'] for item in storage['tenants']]:
        storage['tenants'][old_tenant] = new_tenant
    storage.sync()
    return old_tenant

