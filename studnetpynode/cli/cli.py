import json
import sys

from studnetpynode.main import main
from studnetpynode.storage import PersistentStorage, Formats
from studnetpynode.main import add_tenant, remove_tenant, update_tenant
import click
import configparser
import logging
import os

APP_NAME = 'studnetpynode'
TENANT_FILE = 'studnet.data'
CONFIG_FILE_NAME = APP_NAME + '.conf'


@click.command()
@click.argument("config", type=click.File("r"), required=False)
@click.pass_context
def cli(ctx, config):
    if config is None:
        config_path = os.path.join(click.get_app_dir(APP_NAME), CONFIG_FILE_NAME)
        try:
            config = open(config_path, 'r')
        except FileNotFoundError as e:
            sys.exit(-1)
    cfg = configparser.ConfigParser()
    cfg.read_file(config)
    studnet_ssh = cfg['STUDNET'].get('studnet_ssh', '139.18.143.253')
    host = cfg['MQTT'].get('host', 'localhost')
    port = cfg['MQTT'].getint('port', 1883)
    log_level = cfg['STUDNET'].getint('log_level', logging.INFO)
    storage_path = cfg['STUDNET'].get('studnet_data', os.path.join(click.get_app_dir(APP_NAME), TENANT_FILE))
    ctx.obj = {'storage': PersistentStorage(storage_path, Formats.JSON), 'cfg': cfg}
    main(studnet_ssh, host=host, port=port, log_level=log_level, storage_path=storage_path)


@click.group()
@click.option('-a', '--app-data', type=str, default='.')
@click.pass_context
def tenants(ctx, app_data):
    tenant_data = os.path.join(app_data, TENANT_FILE)
    ctx.obj = PersistentStorage(tenant_data, Formats.JSON)
    res = ctx.obj.load()
    if not res:
        with open(tenant_data, "w+") as f:
            f.write('{}')
        ctx.obj.load()


@tenants.command()
@click.argument('tenant_number', type=int, required=True)
@click.argument('password', type=str, required=True)
@click.pass_obj
def add(storage, tenant_number, password):
    add_tenant(storage, int(tenant_number), password)


@tenants.command()
@click.argument('tenant_number', type=int, required=True)
@click.pass_obj
def remove(storage, tenant_number):
    try:
        remove_tenant(storage, int(tenant_number))
    except IndexError as e:
        return


@tenants.command()
@click.argument('tenant_number', type=int)
@click.argument('new_number', type=int)
@click.argument('password', type=str)
@click.pass_obj
def update(obj, tenant_number, new_number, password):
    update_tenant(obj,
                  {"id": int(tenant_number), "tenant": {'tenant_number': int(new_number), 'password': password}})


@tenants.command()
@click.pass_obj
def list(obj):
    click.echo(json.dumps(obj['tenants'], indent=4))
