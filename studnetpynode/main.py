from studnetpynode.tenant import *
from paho.mqtt.client import Client
from studnetpy import Studnet
from studnetpynode.storage import PersistentStorage, Formats
import json
import logging


TOPICS = {
    "tenants_log": "Studnet/Tenants/Log",
    "tenants": "Studnet/Tenants",
    "status": "Studnet/Status",
    "info": "Studnet/Info",
    "get_tenants": "Studnet/Tenants/Get",
    "add_tenant": "Studnet/Tenants/Add",
    "del_tenant": "Studnet/Tenants/Remove",
    "update_tenant": "Studnet/Tenants/Update",
    "control": "Studnet/Control"
}

client = Client()
storage = PersistentStorage(".", Formats.JSON)
studnet = Studnet()
logger = logging.getLogger("studnetpynode")
studnet_ip = ""


@client.topic_callback(TOPICS["get_tenants"])
def get_tenants(c, userdata, message):
    client.publish(TOPICS["tenants"], payload=str(storage["tenants"]), retain=True)


@client.topic_callback(TOPICS["add_tenant"])
def on_add_tenant(c, userdata, message):
    tenant = json.loads(message.payload.decode())
    add_tenant(storage, **tenant)
    client.publish(TOPICS["tenants_log"], payload=str({"type": "add", "tenant": tenant}))
    info = client.publish(TOPICS["tenants"], payload=str(storage["tenants"]), retain=True)
    logger.debug("Updated tenant list sended to broker. mid: " + str(info.mid) + " rc " + str(info.rc) +
                 " tenant: " + str(storage["tenants"]))


@client.topic_callback(TOPICS["del_tenant"])
def on_remove_tenant(c, userdata, message):
    tenant_number = int(message.payload.decode())
    logger.info("Deleting tenant at index " + str(tenant_number) + " from the tenants' list.")
    try:
        remove_tenant(storage, tenant_number)
    except IndexError as e:
        client.publish(TOPICS["tenants_log"], payload=str({"type": "remove", "tenant_id": -1}))
        logger.error("Could not remove tenant number " + str(tenant_number) + " from the list.", str(e))
        return
    client.publish(TOPICS["tenants_log"], payload=str({"type": "remove", "tenant_id": tenant_number}))
    info = client.publish(TOPICS["tenants"], payload=tenant_number, retain=True)
    logger.debug("Updated tenant list sended to broker. mid: " + str(info.mid) + " rc " + str(info.rc) +
                 " tenants: " + str(storage["tenants"]))


@client.topic_callback(TOPICS["update_tenant"])
def on_update_tenant(c, userdata, message):
    tenant: dict = json.loads(message.payload.decode())
    try:
        tenant_index = update_tenant(storage, tenant)
    except KeyError:
        client.publish(TOPICS["tenants_log"], payload={"type": "update", "tenant_id": -1})
        return
    logger.info("Updating tenant number: " + str(tenant_index))
    logger.debug("Tenant number " + str(tenant_index) + ": " + str(storage["tenants"][tenant_index]))
    storage.sync()
    payload = {"tenant_number": tenant_index, **storage["tenants"][tenant_index]}
    info = client.publish(TOPICS["tenants_log"], payload=str({"type": "update", "tenant": payload}))
    logger.debug("Updated tenant list sended to broker. mid: " + str(info.mid) + " rc " + str(info.rc) +
                 " tenants: " + str(storage["tenants"]))


@client.topic_callback(TOPICS["control"])
def on_control_studnet(c, userdata, message):
    try:
        control = json.loads(message.payload.decode())
    except json.JSONDecodeError as e:
        print(e)
        return
    command = control["command"]
    if command == "connect":
        tenant_number = control["tenant_number"]
        tenant = storage["tenants"][tenant_number]
        password = tenant["password"]
        ip = studnet_ip
        studnet.disconnect()
        studnet.connect(tenant_number, ip, "-p", password)
        client.publish(TOPICS["info"], payload=str(studnet.info()), retain=True)
    if command == "disconnect":
        studnet.disconnect()
    client.publish(TOPICS["status"], payload=studnet.status, retain=True)


@client.on_connect()
def on_connected(c, userdata, flags, rc):
    client.publish(TOPICS["tenants"], payload=str(storage.get("tenants", {})), retain=True)
    client.publish(TOPICS["status"], payload=studnet.status, retain=True)


@client.on_disconnect()
def on_disconnect(c, userdata, flags, rc):
    pass


@client.on_subscribe()
def on_subscribe(c, userdata, flags, rc):
    pass


@client.on_unsubscribe()
def on_unsubscribe(c, userdata, flags, rc):
    pass


def main(studnet_ssh, host="localhost", port=1883, storage_path="studnet.data", log_level=logging.INFO):
    studnet_ip = studnet_ssh
    logger.setLevel(log_level)
    storage.file_path = storage_path
    res = storage.load()
    if not res:
        with open(storage_path, "w+") as f:
            f.write('{}')
        storage.load()
    logger.debug("Credentials loaded: " + str(storage.store))

    client.run(host, port)
    logger.info("Studnetpynode is now connected to the broker with ip: " + host + " and port " + str(port))

