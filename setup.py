from setuptools import setup
from setuptools import find_packages

setup(
    name='studnetpynode',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "studnetpy==1.0.1",
        "paho-mqtt",
        "Click"
    ],
    dependency_links=[
        "git+ssh://git@git.informatik.uni-leipzig.de/mc19roxy/studnetpy.git@1.0.1#egg=studnetpy"
    ],
    entry_points='''
            [console_scripts]
            studnetpynode=studnetpynode.cli.cli:cli
            studnet-tenant=studnetpynode.cli.cli:tenants
        ''',
    url='',
    license='',
    author='Max',
    author_email='',
    description='MQTT node for studnet'
)
